﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GVC.Entities
{
    public class PopulationTransaction
    {
        public string PopulationId { get; set; }
        public List<string> Population { get; set; }
        public string Timestamp { get; set; }
    }
}
