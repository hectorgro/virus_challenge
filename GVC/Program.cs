﻿using GVC.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace GVC
{
    class Program
    {
        private static readonly string URL_INITIAL = "https://9zld4zwegj.execute-api.us-east-1.amazonaws.com/dev/challenge/start";
        private static readonly string URL_SUBMIT = "https://9zld4zwegj.execute-api.us-east-1.amazonaws.com/dev/challenge/submission";
        private static readonly string JSON_CONTENT_TYPE = "application/json";
        private static readonly string T_SEQUENCE = "T";
        private static readonly string A_SEQUENCE = "A";
        private static readonly string C_SEQUENCE = "C";
        private static readonly string G_SEQUENCE = "G";        

        static void Main(string[] args)
        {
            double sickCount = 0;
            string populationString = HttpPost(URL_INITIAL, "{\"email\":\"hector.guerrero.1325@gmail.com\"}");
            PopulationTransaction population = GetPopulation(populationString);

            foreach (string patientUrl in population.Population)
            {
                Console.WriteLine("start reading patient dna...");
                var patientDNA = StreamToPatient(HttpGet(patientUrl));

                if (patientDNA != null)
                {
                    sickCount += IsPatientSick(patientDNA) ? 1 : 0;
                }

                Console.WriteLine("end reading patient dna");
            }

            double sicknessPercentage = (sickCount / population.Population.Count) * 100;            
            string postResults = string.Format("\"populationId\":\"{0}\", \"sicknessPercentage\": {1}", population.PopulationId, Convert.ToInt32(Math.Floor(sicknessPercentage)));
            postResults = "{" + postResults + "}";

            string results = HttpPost(URL_SUBMIT, postResults);
            
            Console.WriteLine("PopulationId: {0}", population.PopulationId);
            Console.WriteLine("SicknessPercentage: {0}", Math.Floor(sicknessPercentage));
            Console.WriteLine("results {0}", results);
            Console.ReadKey();
        }

        private static PopulationTransaction GetPopulation(string response)
        {
            PopulationTransaction population = JsonConvert.DeserializeObject<PopulationTransaction>(response);
            return population;
        }

        private static Stream HttpGet(string uri)
        {
            var request = WebRequest.Create(uri);
            request.Method = WebRequestMethods.Http.Get;
            request.ContentType = JSON_CONTENT_TYPE;

            var response = (HttpWebResponse)request.GetResponse();
            return response.GetResponseStream();
        }

        private static string HttpPost(string url, string payload)
        {            
            string response = null;
            using (var client = new WebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = JSON_CONTENT_TYPE;
                response = client.UploadString(url, WebRequestMethods.Http.Post, payload);
            }
            return response;
        }

        private static Dictionary<string, int> StreamToPatient(Stream response)
        {
            Dictionary<string, int> count = new Dictionary<string, int>();

            using (var reader = new StreamReader(response))
            {
                string dna;
                while ((dna = reader.ReadLine()) != null)
                {
                    var analizedData = AnalizeDNAString(dna);

                    foreach (KeyValuePair<string, int> data in analizedData)
                    {
                        if (count.ContainsKey(data.Key))
                        {
                            count[data.Key] += data.Value;
                        }
                        else
                        {
                            count.Add(data.Key, data.Value);
                        }
                    }
                }
            }

            return count;
        }

        private static Dictionary<string, int> AnalizeDNAString(string patientDNA)
        {
            return patientDNA.GroupBy(x => x).ToDictionary(group => group.Key.ToString(), group => group.Count());
        }

        private static bool IsPatientSick(Dictionary<string, int> patientData)
        {
            if (patientData.ContainsKey(T_SEQUENCE))
            {
                if (patientData[T_SEQUENCE] > patientData[A_SEQUENCE] &&
                    patientData[T_SEQUENCE] > patientData[C_SEQUENCE] &&
                    patientData[T_SEQUENCE] > patientData[G_SEQUENCE])
                {
                    return true;
                }
            }
            return false;
        }
    }
}
